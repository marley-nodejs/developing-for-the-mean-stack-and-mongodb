# Developing-for-the-MEAN-Stack-and-MongoDB
[Lynda] Developing for the MEAN Stack and MongoDB [ENG, 2015]


    npm install -g express-generator
    express . --hbs
    npm install

    npm rm serve-favicon --save

___

## 02. Managing Users and Security

### 010 Composing views with Handlebars

    npm start

http://localhost:3000/  
http://localhost:3000/users/create

Создаем пользователя.


### 011 Handling form submissions in Express

### 012 Creating an object model with Mongoose

    npm install --save mongoose

### 013 Persisting objects with Mongoose

    mongo rtr
    db.users.find()

http://localhost:3000/orders


### 014 Validating Mongoose

https://github.com/chriso/validator.js

http://localhost:3000/users/create

Проверка на уровне базы данных:  
1) Провека заполнения полей.  
2) Проверка уникальность email.  


### 015 Configuring authentication with Passport

    npm install --save passport
    npm install --save passport-local
    npm install --save express-session


### 016 Authenticating users and securing routes

http://localhost:3000/orders

должна быть недоступна для незарегистрированных пользователей.


### 017 Handling unsuccessful login attempts

    npm install --save connect-flash

Сообщение на странице об ошибке при вводе неправильного логина/пароля.


### 018 Implementing strong password hashing with bcrypt

https://github.com/ncb000gt/node.bcrypt.js

    npm install --save bcrypt


Завели нового пользователя, убедились, что password зашифрован.

    mongo rtr
    db.users.find()

удаляем все записи, созданные ранее, кроме последней с зашифрованными паролями.

    db.users.remove({email:{$ne:'mike@secure.com'}})



### 019 Enabling persistent sessions with Node.js and MongoDB

Хранить сессии в базе данных, чтобы после перестартовки сервера они не потерялись.

    npm install --save connect-mongo

    mongo rtr
    show collections

Появилась база sessions

    db.sessions.find()


+ руками добавили в сессию orderId.


___


## 03. Integrating a Mini SPA with AngularJS

### 020 Adding a JSON API route

http://www.ordrx.com/

https://hackfood.ordrx.com/account?welcome=1

Details & Keys

    npm install --save ordrin-api


http://localhost:3000/orders/api/restaurants/

(Похоже, что были временные проблемы с сервисами ordrx, или даже скорее всего не было работающих в это время рестаранов.)


### 021 Configuring client-side routing with AngularJS

    npm install -g bower
    bower init
    bower install --save angular
    bower install --save angular-route


### 022 Creating controllers and views in Angular

http://localhost:3000/orders#/restaurants


### 023 Fetching and binding data with AngularJS


http://localhost:3000/orders#/restaurants


### 024 Creating a custom service in AngularJS

### 025 Using route parameters and nested data binding

### 026 Integrating a modal pop-up service

    bower install --save ngDialog

### 027 Adding view modal behaviors in AngularJS


___


## 04. Putting the Pieces Together


### 028 Using schemas with nested fields in Mongoose

### 029 Managing documents with nested fields

    mongo rtr
    show collections
    db.orders.find()


### 030 Adding basic form validation in AngularJS

http://localhost:3000/orders#/payment


### 031 Adding custom form validation in AngularJS

https://gist.github.com/ShirtlessKirk/2134376

### 032 Preparing a guest order for Ordrx


### 033 Completing the delivery order

(Не все рестораны могут принимать заказы. Поэтому возможна ошибка на этом шаге.)
В любом случае, данный шаг у меня не заработал.

{ msg: { [Error: cannot POST /o/28351 (500)] status: 500, method: 'POST', path: '/o/28351' },
  body: {} }


___

## 05. Deployment

### 034 Setting up a Heroku app

Создали проект в heroku с названием "restaurant-to-room"

    heroku login
    heroku git:remote -a restaurant-to-room
    git add .
    git commit -m "add checkout SPA"
    git push heroku master


    035 Using the Heroku MongoLab add-on

    heroku addons:add mongolab
    heroku config:set NODE_ENV=production
    heroku config:get NODE_ENV

    heroku config:get MONGOLAB_URI


### 036 Using gulp for minification

    npm install --save-dev gulp
    npm install --save-dev gulp-concat
    npm install --save-dev gulp-uglify

    gulp
